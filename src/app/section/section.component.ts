import { Component, Input, OnInit } from '@angular/core';
import { ButtonComponent } from "../button/button.component";

@Component({
    selector: 'app-section',
    standalone: true,
    templateUrl: './section.component.html',
    styleUrl: './section.component.scss',
    imports: [ButtonComponent]
})
export class SectionComponent  {
  @Input() title: string | undefined;
  @Input() description: string | undefined;
  @Input() image: string | undefined;
  @Input() buttonContent: string | undefined;


}
