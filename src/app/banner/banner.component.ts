import { Component, Input } from '@angular/core';
import { ButtonComponent } from "../button/button.component";

@Component({
    selector: 'app-banner',
    standalone: true,
    templateUrl: './banner.component.html',
    styleUrl: './banner.component.scss',
    imports: [ButtonComponent]
})
export class BannerComponent {
    @Input() buttonContent: string = "";

}
