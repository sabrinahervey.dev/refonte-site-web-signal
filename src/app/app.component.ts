import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from "./header/header.component";
import { BannerComponent } from "./banner/banner.component";
import { ButtonComponent } from "./button/button.component";
import { SectionComponent } from "./section/section.component";
import { CardsComponent } from "./cards/cards.component";
import { FooterComponent } from "./footer/footer.component";



@Component({
    selector: 'app-root',
    standalone: true,
    templateUrl: './app.component.html',
    styleUrl: './app.component.scss',
    imports: [
        CommonModule,
        HeaderComponent,
        BannerComponent,
        ButtonComponent,
        SectionComponent,
        CardsComponent,
        FooterComponent
    ]
})
export class AppComponent {
  bannerButton ="Obtenir Signal"


  sectionsData = [
    {
      title: "Pourquoi utiliser Signal ?",
      description: "Explorez ci-dessous afin de découvrir pourquoi Signal est une messagerie simple, puissante et sécurisée",
      image: undefined,
      buttonContent : undefined
    },
    {
      title: "Partager sans insécurité",
      description: "Un chiffrement de bout en bout de pointe (propulsé par le protocole Signal à code source ouvert) assure la sécurité de vos conversations. Nous ne pouvons ni lire vos messages ni écouter vos appels, et personne d’autre que vous ne peut le faire. La confidentialité n’est pas proposée en option, c’est simplement la façon dont Signal fonctionne. Pour tous les messages, pour tous les appels, tout le temps.",
      image: "../../assets/animation.png",
      buttonContent: undefined
    },
    {
      title: "Aucune publicité, aucun traqueur, vraiment.",
      description: "Dans Signal, il n’y a aucune publicité, aucun vendeur partenaire, ni aucun système de suivi inquiétant. Vous pouvez vous consacrer à partager les moments importants avec les personnes qui comptent pour vous.",
      image: "../../assets/No-Ads.png",
      buttonContent: undefined
    },
    {
      title: "Gratuit pour tous",
      description: "Signal est un organisme à but non lucratif indépendant. Nous ne sommes reliés à aucune entreprise technologique importante et nous ne pourrons jamais être achetés par l’une d’elles. Le développement de notre plateforme est financé par des subventions et des dons de personnes comme vous.",
      image: "../../assets/Nonprofit503.png",
      buttonContent: "Faire un don à Signal"
    },
  ];
  cardsData = [
    {
      title: "Dites ce qui vous plaît",
      description: "Partagez gratuitement des textes, des messages vocaux, des photos, des vidéos et des GIF. Signal utilise la connexion de données de votre téléphone afin de vous éviter les frais relatifs à l’envoi de texto et de message multimédia.",
      image: "../../assets/Media.png"
    },
    {
      title: "Exprimez-vous librement",
      description: "Passez des appels vocaux ou vidéo clairs avec des personnes qui vivent à l’autre bout de la ville ou du monde, sans frais supplémentaires",
     image: "../../assets/Calls.png"
    },
    {
      title: "Respectez votre vie privée",
      description: "Avec nos stickers chiffrés, exprimez-vous encore plus librement. Vous êtes même libre de créer et de partager vos propres packs de stickers.",
      image: "../../assets/Stickers.png"
    },
    {
      title: "Rassemblez-vous avec les groupes",
      description: "Avec les conversations de groupe, restez facilement en contact avec votre famille, vos amis et vos collègues.",
      image: "../../assets/Groups.png"
    },
  ];

}
